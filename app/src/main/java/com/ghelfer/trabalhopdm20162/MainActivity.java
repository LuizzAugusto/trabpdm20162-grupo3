package com.ghelfer.trabalhopdm20162;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText matricula;
    private EditText senha;
    private String resposta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(Bundle.EMPTY);
        setContentView(R.layout.activity_main);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if(netInfo == null) {
            Toast.makeText(this, "Error, no internet connection!", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            matricula = (EditText) findViewById(R.id.matricula);
            senha = (EditText) findViewById(R.id.senha);

            resposta =
                    (new Rest("http://ghelfer.net/pdm/ListaPessoa.aspx", Rest.POST, Rest.HTTP, Rest.READ)).getResponse();
        }
    }

    protected void entrarClick(View view) {

        String matricula = this.matricula.getText().toString();
        String senha = this.senha.getText().toString();

        if (matricula.isEmpty()) {
            Toast.makeText(this, "Preencha todos os campos", Toast.LENGTH_SHORT).show();
        } else {
            try {
                JSONObject json = new JSONObject(resposta);
                JSONArray jsonArray = json.getJSONArray("Pessoa");
                boolean flag = false;

                for (int i = 0; i < jsonArray.length() & !flag; i++) {
                    String m = jsonArray.getJSONObject(i).getString("matricula");
                    flag = matricula.equals(m);

                    if (flag) {
                        long id = Long.parseLong(jsonArray.getJSONObject(i).getString("idpessoa"));

                        Intent intent = new Intent(MainActivity.this, Activity6.class);
                        intent.putExtra("PESSOA_ID", id);

                        startActivity(intent);
                    }
                }

                if (!flag) {
                    Toast.makeText(this, "Matrícula ou senha inválidos", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
