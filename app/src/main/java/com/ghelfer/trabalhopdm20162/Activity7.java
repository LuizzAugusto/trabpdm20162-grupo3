package com.ghelfer.trabalhopdm20162;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Activity7 extends AppCompatActivity {
    private final static String[] DE = {
            KeysJSON.ATIVIDADE_ID, KeysJSON.ATIVIDADE_INSCRICAO_PESSOA_ID,
            KeysJSON.ATIVIDADE_TITULO, KeysJSON.ATIVIDADE_DESCRICAO, KeysJSON.ATIVIDADE_MINISTRANTE,
            KeysJSON.ATIVIDADE_LOCAL, KeysJSON.ATIVIDADE_DATA_INICIO, KeysJSON.ATIVIDADE_DATA_FIM,
            KeysJSON.ATIVIDADE_LATITUDE, KeysJSON.ATIVIDADE_LONGITUDE
    };

    private final static int[] PARA = {
            R.id.item_id, R.id.item_pessoaId,
            R.id.item_titulo, R.id.item_descricao, R.id.item_ministrante, R.id.item_local,
            R.id.item_data_ini, R.id.item_data_fim, R.id.item_latitude, R.id.item_longitude
    };

    private ListView listViewAtividades;
    private List<Map<String, String>> listAtividades;

    private long pessoaId;
    private long eventoId;
    private boolean inscritoEvento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_7);
        setTitle("Evento e Atividades");

        listViewAtividades = (ListView) findViewById(R.id.listViewAtividades);

        pessoaId = getIntent().getLongExtra(KeysJSON.EVENTO_INSCRICAO_PESSOA_ID, -1);
        eventoId = getIntent().getLongExtra(KeysJSON.EVENTO_ID, -1);

        inscritoEvento = getIntent().getBooleanExtra(KeysJSON.EVENTO_INSCRICAO_INSCRITO, false);

        exibeEvento();
        exibeAtividades();
    }

    // Exibe nome e descricao do Evento
    private void exibeEvento() {
        String titulo = getIntent().getStringExtra(KeysJSON.EVENTO_TITULO);
        String descricao = getIntent().getStringExtra(KeysJSON.EVENTO_DESCRICAO);

        ((TextView) findViewById(R.id.eventoTitulo)).setText(titulo);
        ((TextView) findViewById(R.id.eventoDescr)).setText(descricao);
    }

    // Exibe atividades do Evento
    private void exibeAtividades() {
        String resposta;
        Map<String, String> map;

        listAtividades = new ArrayList<>();

        resposta = (new Rest("http://ghelfer.net/pdm/ListaAtividade.aspx", Rest.POST, Rest.HTTP, Rest.READ)).
                getResponse(); // Faz um request para http://ghelfer.net/pdm/ListaAtividade.aspx e retorna a resposta.

        try {
            // Quebra do JSON ///////////////////////////////////////////////////////////////////////
            JSONObject json = new JSONObject(resposta);
            JSONArray jsonArray = json.getJSONArray(KeysJSON.ATIVIDADE);

            for (int i = 0; i < jsonArray.length(); i++) {  // For para gerar HashMaps
                JSONObject object = jsonArray.getJSONObject(i);
                String status = object.getString(KeysJSON.ATIVIDADE_STATUS);
                String eventoid = object.getString(KeysJSON.ATIVIDADE_EVENTO_ID);

                if (!status.equals("D") & eventoid.equals("" + this.eventoId)) { // Testa se o evento está deletado
                    int atividadeid = object.getInt(KeysJSON.ATIVIDADE_ID);

                    map = geraMap(
                            "" + atividadeid,
                            object.getString(KeysJSON.ATIVIDADE_TITULO),
                            object.getString(KeysJSON.ATIVIDADE_DESCRICAO),
                            object.getString(KeysJSON.ATIVIDADE_MINISTRANTE),

                            object.getString(KeysJSON.ATIVIDADE_LOCAL),
                            object.getString(KeysJSON.ATIVIDADE_DATA_INICIO),
                            object.getString(KeysJSON.ATIVIDADE_DATA_FIM),
                            eventoid,

                            object.getString(KeysJSON.EVENTO_LATITUDE),
                            object.getString(KeysJSON.EVENTO_LONGITUDE),
                            status);

                    // FIM Quebra do JSON ///////////////////////////////////////////////////////////

                    listAtividades.add(map); // Adicionando a lista de Eventos
                }
            }

            SimpleAdapter adapter = new AdaptadorAtividade(this, listAtividades, R.layout.atividade_layout,
                    DE, PARA, (new Rest("http://ghelfer.net/pdm/ListaInscricaoAtividade.aspx", Rest.POST, Rest.HTTP, Rest.READ)).
                    getResponse(), inscritoEvento);
            listViewAtividades.setAdapter(adapter);  // Adicionando a ListView de Eventos

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Gera HashMap do Evento
    private Map<String, String> geraMap(String id, String titulo, String descricao, String ministrante, String local,
                                        String dt_ini, String dt_fim, String eventoid, String lat,
                                        String lon, String status) {
        Map<String, String> map = new HashMap<>();

        map.put(KeysJSON.ATIVIDADE_ID, id);
        map.put(KeysJSON.ATIVIDADE_TITULO, titulo);
        map.put(KeysJSON.ATIVIDADE_DESCRICAO, descricao);
        map.put(KeysJSON.ATIVIDADE_MINISTRANTE, ministrante);

        map.put(KeysJSON.ATIVIDADE_LOCAL, local);
        map.put(KeysJSON.ATIVIDADE_DATA_INICIO, Activity6.getDate(dt_ini));
        map.put(KeysJSON.ATIVIDADE_DATA_FIM, Activity6.getDate(dt_fim));
        map.put(KeysJSON.ATIVIDADE_EVENTO_ID, eventoid);

        map.put(KeysJSON.ATIVIDADE_LATITUDE, lat);
        map.put(KeysJSON.ATIVIDADE_LONGITUDE, lon);
        map.put(KeysJSON.ATIVIDADE_STATUS, status);
        map.put(KeysJSON.ATIVIDADE_INSCRICAO_PESSOA_ID, "" + pessoaId);

        return map;
    }

    private class AdaptadorAtividade extends SimpleAdapter {
        private String resposta;
        private boolean inscritoEvento;

        public AdaptadorAtividade(Context context, List<? extends Map<String, ?>> data, int resource,
                                  String[] from, int[] to, String resposta, boolean inscritoEvento) {
            super(context, data, resource, from, to);

            this.resposta = resposta;
            this.inscritoEvento = inscritoEvento;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            final long atividadeId = Long.parseLong(((TextView) view.findViewById(R.id.item_id)).getText().toString());
            final long pessoaId = Long.parseLong(((TextView) view.findViewById(R.id.item_pessoaId)).getText().toString());

            boolean par = (position % 2) == 0;
            int corPadrao = view.getDrawingCacheBackgroundColor();

            Button btnInscreverAtividade = (Button) view.findViewById(R.id.item_btnInscreverAtividade);

            if (par)
                view.setBackgroundColor(Color.parseColor("#C6E2FF") /* Azul Claro */);
            else
                view.setBackgroundColor(corPadrao);

            if (inscritoAtividade(atividadeId, pessoaId)) {
                btnInscreverAtividade.setEnabled(false);
                btnInscreverAtividade.setText("inscrito");
            } else {
                btnInscreverAtividade.setEnabled(true);
                btnInscreverAtividade.setText("inscrever-se em atividade");
            }

            btnInscreverAtividade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String[] values = {
                            "idatividade", "" + atividadeId,
                            "idpessoa", "" + pessoaId
                    };

                    new Rest("http://ghelfer.net/pdm/InsereInscricaoAtividade.aspx", Rest.POST, Rest.HTTP, Rest.WRITE).getResponse(values);

                    exibeEvento();
                    exibeAtividades();
                }
            });

            view.findViewById(R.id.item_btnDetalhesDaAtividade).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Activity7.this, Activity9.class));
                }
            });

            return view;
        }

        private boolean inscritoAtividade(long atividadeId, long pessoaId) {
            if (inscritoEvento)
                try {
                    JSONObject json = new JSONObject(resposta);
                    JSONArray jsonArray = json.getJSONArray(KeysJSON.ATIVIDADE_INSCRICAO);
                    JSONObject object = null;
                    boolean flag = false;

                    for (int i = 0; i < jsonArray.length() & !flag; i++) {
                        object = jsonArray.getJSONObject(i);

                        flag = object.getString(KeysJSON.ATIVIDADE_INSCRICAO_ATIVIDADE_ID).equals("" + atividadeId);
                        flag &= object.getString(KeysJSON.ATIVIDADE_INSCRICAO_PESSOA_ID).equals("" + pessoaId);
                    }

                    return flag;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            return true;
        }
    }
}
