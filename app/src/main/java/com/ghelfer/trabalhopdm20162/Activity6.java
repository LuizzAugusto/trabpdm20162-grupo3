package com.ghelfer.trabalhopdm20162;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Activity6 extends AppCompatActivity {
    private final static int ATUALIZAR = 1;

    private final static String[] DE = {
            KeysJSON.EVENTO_TITULO, KeysJSON.EVENTO_DESCRICAO, KeysJSON.EVENTO_LOCAL, KeysJSON.EVENTO_LATITUDE,
            KeysJSON.EVENTO_LONGITUDE, KeysJSON.EVENTO_DEPARTAMENTO, KeysJSON.EVENTO_CURSO, KeysJSON.EVENTO_DATA_INICIO,
            KeysJSON.EVENTO_DATA_FIM, KeysJSON.EVENTO_CUSTO, KeysJSON.EVENTO_INSCRICAO_INSCRITO, KeysJSON.EVENTO_INSCRICAO_PAGAMENTO
    };
    private final static int[] PARA = {
            R.id.item_titulo, R.id.item_descricao, R.id.item_local,
            R.id.item_latitude, R.id.item_longitude, R.id.item_departamento, R.id.item_curso,
            R.id.item_data_ini, R.id.item_data_fim, R.id.item_custo, R.id.item_inscrito, R.id.item_pagamento
    };

    private ListView listViewEventos;
    private List<Map<String, String>> listEventos;

    private long pessoaId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Eventos");
        setContentView(R.layout.activity_6);

        listViewEventos = (ListView) findViewById(R.id.listViewEventos);

        pessoaId = getIntent().getLongExtra("PESSOA_ID", -1);

        exibeEventos();
    }

    // Exibe eventos na listViewEventos
    private void exibeEventos() {
        Map<String, String> map = null;

        String resposta =
                (new Rest("http://ghelfer.net/pdm/ListaEvento.aspx", Rest.POST, Rest.HTTP, Rest.READ)).
                        getResponse(); // Faz um request para http://ghelfer.net/pdm/ListaEvento.aspx e retorna a resposta.

        listEventos = new ArrayList<>();

        try {
            // Quebra do JSON ///////////////////////////////////////////////////////////////////////
            JSONObject json = new JSONObject(resposta);
            JSONArray jsonArray = json.getJSONArray(KeysJSON.EVENTO);

            for (int i = 0; i < jsonArray.length(); i++) {  // For para gerar HashMaps
                JSONObject object = jsonArray.getJSONObject(i);
                String status = object.getString(KeysJSON.EVENTO_STATUS);

                if (!status.equals("D")) { // Testa se o evento está deletado
                    map = geraMap(
                            object.getString(KeysJSON.EVENTO_ID),
                            object.getString(KeysJSON.EVENTO_TITULO),
                            object.getString(KeysJSON.EVENTO_DESCRICAO),
                            object.getString(KeysJSON.EVENTO_LOCAL),

                            object.getString(KeysJSON.EVENTO_LONGITUDE),
                            object.getString(KeysJSON.EVENTO_LATITUDE),
                            object.getString(KeysJSON.EVENTO_DEPARTAMENTO),
                            object.getString(KeysJSON.EVENTO_CURSO),

                            object.getString(KeysJSON.EVENTO_DATA_INICIO),
                            object.getString(KeysJSON.EVENTO_DATA_FIM),
                            object.getString(KeysJSON.EVENTO_CUSTO),
                            status);

                    // FIM Quebra do JSON ///////////////////////////////////////////////////////////

                    listEventos.add(map); // Adicionando a lista de Eventos
                }
            }

            boolean inscrito = false;

            try {
                inscrito = map.get(KeysJSON.EVENTO_INSCRICAO_INSCRITO).equals("sim");
            } catch (NullPointerException e) {}

            SimpleAdapter adapter = new AdaptadorEvento(this, listEventos, R.layout.evento_layout,
                    DE, PARA, inscrito);
            listViewEventos.setAdapter(adapter);  // Adicionando a ListView de Eventos


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Gera HashMap do Evento
    private Map<String, String> geraMap(String id, String titulo, String descricao, String local, String lat,
                                        String lon, String depto, String curso, String dt_ini, String dt_fim,
                                        String custo, String status) {
        Map<String, String> map = new HashMap<>();
        String resposta =
                (new Rest("http://ghelfer.net/pdm/ListaInscricaoEvento.aspx", Rest.POST, Rest.HTTP, Rest.READ))
                        .getResponse(); // Faz um request para http://ghelfer.net/pdm/ListaInscricaoEvento.aspx e retorna a resposta.

        map.put(KeysJSON.EVENTO_ID, id);
        map.put(KeysJSON.EVENTO_TITULO, titulo);
        map.put(KeysJSON.EVENTO_DESCRICAO, descricao);
        map.put(KeysJSON.EVENTO_LOCAL, local);

        map.put(KeysJSON.EVENTO_LATITUDE, lat);
        map.put(KeysJSON.EVENTO_LONGITUDE, lon);
        map.put(KeysJSON.EVENTO_DEPARTAMENTO, depto);
        map.put(KeysJSON.EVENTO_CURSO, curso);

        map.put(KeysJSON.EVENTO_DATA_INICIO, getDate(dt_ini));
        map.put(KeysJSON.EVENTO_DATA_FIM, getDate(dt_fim));
        map.put(KeysJSON.EVENTO_CUSTO, custo);
        map.put(KeysJSON.EVENTO_STATUS, status);

        if (pessoaId != -1) {
            try {
                JSONObject json = new JSONObject(resposta);
                JSONArray jsonArray = json.getJSONArray(KeysJSON.EVENTO_INSCRICAO);
                JSONObject object = null;
                boolean flag = false;


                for (int i = 0; i < jsonArray.length() & !flag; i++) {
                    object = jsonArray.getJSONObject(i);

                    flag = object.getString(KeysJSON.EVENTO_INSCRICAO_EVENTO_ID).equals(id);
                    flag &= object.getString(KeysJSON.EVENTO_INSCRICAO_PESSOA_ID).equals("" + pessoaId);
                }


                if (flag) {
                    String pagamento = object.getString(KeysJSON.EVENTO_INSCRICAO_PAGAMENTO);
                    boolean pago = pagamento.equals("true");

                    if (pago)
                        map.put(KeysJSON.EVENTO_INSCRICAO_PAGAMENTO, "sim");
                    else
                        map.put(KeysJSON.EVENTO_INSCRICAO_PAGAMENTO, "não");

                    map.put(KeysJSON.EVENTO_INSCRICAO_INSCRITO, "sim");
                } else {
                    map.put(KeysJSON.EVENTO_INSCRICAO_INSCRITO, "não");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    public static String getDate(String time) {
        time = time.replaceAll("\\D+", "");
        String date;
        int day, month, year;

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(Long.parseLong(time));

        day = calendar.get(Calendar.DATE);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        date = "" + day + "/";

        if (day < 10)
            date = 0 + date;

        if (month < 10)
            date += 0 + "" + month + "/";
        else
            date += "" + month + "/";

        date += year;

        return date;
    }

    private class AdaptadorEvento extends SimpleAdapter {

        private boolean inscrito;

        public AdaptadorEvento(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to, boolean inscrito) {
            super(context, data, resource, from, to);

            this.inscrito = inscrito;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);

            boolean par = (position % 2) == 0;
            int corPadrao = view.getDrawingCacheBackgroundColor();

            if (par)
                view.setBackgroundColor(Color.parseColor("#C6E2FF") /* Azul Claro */);
            else
                view.setBackgroundColor(corPadrao);

            Button btnInscreverEvento = (Button) view.findViewById(R.id.item_btnInscreverEvento);
            Button btnInscreverAtividades = (Button) view.findViewById(R.id.item_btnInscreverAtividades);


            view.findViewById(R.id.item_inscr).setVisibility(View.GONE);
            view.findViewById(R.id.item_inscrito).setVisibility(View.GONE);

            if(inscrito) {
                btnInscreverEvento.setEnabled(false);
                btnInscreverAtividades.setVisibility(View.VISIBLE);
                btnInscreverEvento.setText("inscrito");

                view.findViewById(R.id.item_pago).setVisibility(View.VISIBLE);
                view.findViewById(R.id.item_pagamento).setVisibility(View.VISIBLE);
            } else {
                btnInscreverEvento.setEnabled(true);
                btnInscreverAtividades.setVisibility(View.INVISIBLE);

                view.findViewById(R.id.item_pago).setVisibility(View.GONE);
                view.findViewById(R.id.item_pagamento).setVisibility(View.GONE);
                btnInscreverEvento.setText("inscrever-se em evento");
            }

            btnInscreverEvento.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toActivity7(v, true);
                }
            });

            btnInscreverAtividades.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toActivity7(v, inscrito);
                }
            });

            view.findViewById(R.id.item_btnDetalhesDoEvento).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Activity6.this, Activity8.class));
                }
            });

            return view;
        }
    }

    private void toActivity7(View view, boolean inscrito) {
        Map<String, String> map = listEventos.get(view.getVerticalScrollbarPosition());

        long eventoId = Long.parseLong(map.get(KeysJSON.EVENTO_ID));

        String[] values = {
                "idevento", "" + eventoId,
                "idpessoa", "" + pessoaId
        };

        new Rest("http://ghelfer.net/pdm/InsereInscricaoEvento.aspx", Rest.POST, Rest.HTTP, Rest.WRITE).getResponse(values);

        Intent intent;

        intent = new Intent(Activity6.this, Activity6.class);
        intent.putExtra("PESSOA_ID", pessoaId);

        startActivity(intent);

        intent = new Intent(Activity6.this, Activity7.class);

        intent.putExtra(KeysJSON.EVENTO_INSCRICAO_PESSOA_ID, pessoaId);
        intent.putExtra(KeysJSON.EVENTO_INSCRICAO_INSCRITO, inscrito);

        intent.putExtra(KeysJSON.EVENTO_ID, Long.parseLong(map.get(KeysJSON.EVENTO_ID)));
        intent.putExtra(KeysJSON.EVENTO_TITULO, map.get(KeysJSON.EVENTO_TITULO));
        intent.putExtra(KeysJSON.EVENTO_DESCRICAO, map.get(KeysJSON.EVENTO_DESCRICAO));
        startActivity(intent);

        finish();
    }
}
