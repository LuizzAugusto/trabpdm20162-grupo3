package com.ghelfer.trabalhopdm20162;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by luizz on 17/11/16.
 */

public class Rest extends AsyncTask<String, String, String> {
    public final static String GET = "GET ";
    public final static String POST = "POST";

    public final static boolean HTTP = false;
    public final static boolean HTTPS = true;

    public final static boolean READ = false;
    public final static boolean WRITE = true;

    private String urlStr;
    private String method;

    private boolean https;
    private boolean write;

    // Cosntrutor
    public Rest(String urlStr, String method, boolean https, boolean write) {
        this.urlStr = urlStr;
        this.method = method;

        this.https = https;
        this.write = write;
    }

    // Método obrigatório, use a função execute().get()
    @Override
    protected String doInBackground(String... strings) {
        try {
            URL url = new URL(urlStr);

            if (https) {
                HttpsURLConnection connection;
                connection = (HttpsURLConnection) url.openConnection();

                insert(connection, strings);

                return request(connection);
            } else {
                HttpURLConnection connection;
                connection = (HttpURLConnection) url.openConnection();

                insert(connection, strings);

                return request(connection);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Implementa request http
    private String request(HttpURLConnection connection) throws IOException {
        InputStream inputStream;

        int resCode;

        connection.connect();
        resCode = connection.getResponseCode();

        if (resCode == HttpURLConnection.HTTP_OK) {
            inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream, "utf-8"), 8);

            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            inputStream.close();

            return sb.toString();
        }

        return null;
    }

    private void insert(HttpURLConnection connection, String[] strings) throws IOException {
        if (write) {
            ContentValues values = new ContentValues();

            for (int i = 0; i < strings.length; i += 2) {
                String key = strings[i];
                String name = strings[i + 1];

                values.put(key, name);
            }

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod(method);

            OutputStream out = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));

            writer.write(getFormData(values));
            writer.flush();
        }
    }

    private String getFormData(ContentValues values) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, Object> entry : values.valueSet()) {
            if (first)
                first = false;
            else
                sb.append("&");

            sb.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            sb.append("=");
            sb.append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
        }
        Log.i("string", sb.toString());
        return sb.toString();
    }

    public String getResponse() {
        return getResponse(null);
    }

    public String getResponse(String[] strings) {
        try {
            return execute(strings).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }
}
