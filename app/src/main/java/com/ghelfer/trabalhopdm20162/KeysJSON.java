package com.ghelfer.trabalhopdm20162;

/**
 * Created by luizz on 29/11/16.
 */

public class KeysJSON {
    // Keys do JSON em http://ghelfer.net/pdm/ListaEvento.aspx
    public final static String EVENTO = "Evento";

    public final static String EVENTO_ID = "idevento";
    public final static String EVENTO_TITULO = "titulo";
    public final static String EVENTO_DESCRICAO = "descricao";
    public final static String EVENTO_LOCAL = "local";
    public final static String EVENTO_LATITUDE = "lat";
    public final static String EVENTO_LONGITUDE = "lon";
    public final static String EVENTO_DEPARTAMENTO = "depto";
    public final static String EVENTO_CURSO = "curso";
    public final static String EVENTO_DATA_INICIO = "dt_ini";
    public final static String EVENTO_DATA_FIM = "dt_fim";
    public final static String EVENTO_CUSTO = "custo";
    public final static String EVENTO_STATUS = "status";


    // Keys do JSON em http://ghelfer.net/pdm/ListaAtividade.aspx
    public final static String ATIVIDADE = "Atividade";

    public final static String ATIVIDADE_ID = "idatividade";
    public final static String ATIVIDADE_TITULO = "titulo";
    public final static String ATIVIDADE_DESCRICAO = "descricao";
    public final static String ATIVIDADE_MINISTRANTE = "ministrante";
    public final static String ATIVIDADE_LOCAL = "local";
    public final static String ATIVIDADE_DATA_INICIO = "dt_ini";
    public final static String ATIVIDADE_DATA_FIM = "dt_fim";
    public final static String ATIVIDADE_EVENTO_ID = "eventoid";
    public final static String ATIVIDADE_LATITUDE = "lat";
    public final static String ATIVIDADE_LONGITUDE = "lon";
    public final static String ATIVIDADE_STATUS = "status";

    // Keys do JSON em http://ghelfer.net/pdm/ListaInscricaoAtividade.aspx
    public final static String ATIVIDADE_INSCRICAO = "InscrAtiv";

    public final static String ATIVIDADE_INSCRICAO_ID = "idinscricaoatividade";
    public final static String ATIVIDADE_INSCRICAO_ATIVIDADE_ID = "atividadeid";
    public final static String ATIVIDADE_INSCRICAO_PESSOA_ID = "pessoaid";
    public final static String ATIVIDADE_INSCRICAO_FREQUENCIA = "frequencia";
    public final static String ATIVIDADE_INSCRICAO_AVALIACAO = "avaliacao";

    // Keys do JSON em http://ghelfer.net/pdm/ListaInscricaoEvento.aspx
    public final static String EVENTO_INSCRICAO = "InscrEvento";

    public final static String EVENTO_INSCRICAO_ID = "idinscricaoevento";
    public final static String EVENTO_INSCRICAO_EVENTO_ID = "eventoid";
    public final static String EVENTO_INSCRICAO_PESSOA_ID = "pessoaid";
    public final static String EVENTO_INSCRICAO_PAGAMENTO = "pagamento";
    public final static String EVENTO_INSCRICAO_AVALIACAO = "avaliacao";

    // Não é key JSON, mas é usado como key.
    public final static String EVENTO_INSCRICAO_INSCRITO = "inscrito";
    public final static String BUTTON_INSCREVER_EVENTO = "btnincreverevento";
    public final static String BUTTON_DETALHES_EVENTO = "btndetalhesevento";
}
